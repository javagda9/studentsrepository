<%@ page import="com.sda.university.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="java.util.Base64" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 6/30/18
  Time: 1:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<%!
//    // zamienia pojedynczego studenta na string'a z ';' pomiędzy polami i ':' na koncu
//    public String studentToString(Student student) {
//        return student.getFirstName() + ";" + student.getLastName() + ";" + student.getAge() + ";" + student.getIndex() + "/";
//    }
//
//    // konwertuje całą listę studentów na ciąg (string)
//    public String convertToString(List<Student> students) {
//        StringBuilder builder = new StringBuilder();
//        for (Student student : students) {
//            builder.append(studentToString(student));
//        }
//
//        String result = builder.toString();
//        if (result.length() > 1) {
//            return result.substring(0, result.length() - 1);
//        }
//        return result;
//    }
%>
<%
    // mam listę w sesji
    List<Student> students = (List<Student>) session.getAttribute("studenty");

    // tworze obiekt zdolny do serializacji
    Gson gson = new Gson();

    // po otrzymaniu json'a zamieniamy go na base64
    // cookie nie moze zawierac znakow specjalnych
    byte[] encodedBytes = Base64.getEncoder().encode(gson.toJson(students).getBytes());

    // tworzymy cookie
    Cookie cookie = new Cookie("saved_studenty", new String(encodedBytes));
    cookie.setMaxAge(60 * 60 * 24 * 365);

    // dodajemy cookie
    response.addCookie(cookie);

    // przekierowanie
    response.sendRedirect("lista_studentow.jsp");
%>
</body>
</html>
