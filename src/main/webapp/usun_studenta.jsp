<%@ page import="com.sda.university.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 6/30/18
  Time: 1:12 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Usun studenta</title>
</head>
<body>

<%
    String usuwanyIndex = request.getParameter("usuwany_student_index");

    // pobieranie listy studentów z sesji
    List<Student> students = (List<Student>) session.getAttribute("studenty");

    // usunięcia wybranego studenta
    Iterator<Student> studentIterator = students.iterator();
    while (studentIterator.hasNext()) {
        Student student = studentIterator.next();
        if (student.getIndex().equals(usuwanyIndex)) {
            studentIterator.remove();
            break;
        }
    }

    // zwrócenia listy do sesji
    session.setAttribute("studenty", students);

    // następnie przekierowanie na stronę lista_studentow.jsp
    response.sendRedirect("lista_studentow.jsp");
%>

</body>
</html>
