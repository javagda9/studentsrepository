<%--
 Created by IntelliJ IDEA.
 User: amen
 Date: 6/30/18
 Time: 11:31 AM
 To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Formularz studenta!</title>
</head>
<body>

<%@include file="header.jsp" %>

<%--<%!--%>
<%--String imie = null;--%>
<%--String nazwisko = null;--%>
<%--Integer wiek = null;--%>
<%--String index = null;--%>
<%--%>--%>

<%
    String imie = null;
    String nazwisko = null;
    Integer wiek = null;
    String index = null;
    if (request.getParameter("edited_first_name") != null) {
        imie = request.getParameter("edited_first_name");
    }
    nazwisko = request.getParameter("edited_last_name");

    if (request.getParameter("edited_age") != null) {
        wiek = Integer.parseInt(request.getParameter("edited_age"));
    }

    index = request.getParameter("edited_index");
%>

<% if (index == null) { %>
<form action="dodaj_studenta.jsp" method="post">
        <% } else { %>
    <form action="zapisz_studenta.jsp" method="post">
        <% } %>


        <label for="first_name">Imię:</label>
        <% if (imie == null) { %>
        <input type="text" id="first_name" name="first_name">
        <% } else { %>
        <input type="text" id="first_name" name="first_name" value="<%=imie%>">
        <% } %>
        </br>

        <label for="last_name">Nazwisko:</label>
        <% if (nazwisko == null) { %>
        <input type="text" id="last_name" name="last_name">
        <% } else { %>
        <input type="text" id="last_name" name="last_name" value="<%=nazwisko%>">
        <% } %>
        </br>

        <label for="age">Wiek:</label>
        <% if (wiek == null) { %>
        <input type="number" id="age" name="age">
        <% } else { %>
        <input type="number" id="age" name="age" value="<%=wiek%>">
        <% } %>
        </br>

        <label for="index">Index:</label>
        <% if (index == null) { %>
        <input type="text" id="index" name="index">
        <% } else { %>
        <input type="text" id="index" name="index" value="<%=index%>">
        <% } %>
        </br>

        <% if (index != null) { %>
        <input type="hidden" hidden name="zmieniany_index" value="<%=index%>">
        <% }%>


        <% if (index == null) { %>
        <input type="submit" name="Dodaj" value="Dodaj">
        <% } else { %>
        <input type="submit" name="Dodaj" value="Zapisz">
        <% } %>
    </form>
    <%--Stwórz formularz dla instancji klasy student. Form powinien wywołać metodę POST i przenieśc nas na adres dodaj_studenta.jsp --%>

</body>
</html>
