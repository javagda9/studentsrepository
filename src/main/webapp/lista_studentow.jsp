<%@ page import="com.sda.university.Student" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 6/30/18
  Time: 12:12 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Lista studentów!</title>
</head>
<body>
<%@include file="header.jsp" %>
<%!
    private String getEditAddress(Student student) {
        return "formularz.jsp?" +
                "edited_first_name=" + student.getFirstName() + "&" +
                "edited_last_name=" + student.getLastName() + "&" +
                "edited_age=" + student.getAge() + "&" +
                "edited_index=" + student.getIndex();
    }

    private String getEditControlTag(Student student) {
        return "<a href=" + getEditAddress(student) + ">Edytuj</a>";
    }

    public String getRequestAddress(Student student) {
        return "usun_studenta.jsp?usuwany_student_index=" + student.getIndex();
    }

    public String getRemoveControlTag(Student student) {
        return "<a href=" + getRequestAddress(student) + ">Usun</a>";
    }
%>

<table>
    <%
        /// wylistować w postaci tabeli wszystkich studentów z sesji
        List<Student> students = (List<Student>) session.getAttribute("studenty");
        if (students == null) {
            out.println("Nie ma studentów");
        } else {
            for (int i = 0; i < students.size(); i++) {
                out.print("<tr>");
                out.print("<td>" + students.get(i).getFirstName() + "</td>");
                out.print("<td>" + students.get(i).getLastName() + "</td>");
                out.print("<td>" + students.get(i).getAge() + "</td>");
                out.print("<td>" + students.get(i).getIndex() + "</td>");
                out.print("<td>" + getRemoveControlTag(students.get(i)) + "</td>");
                out.print("<td>" + getEditControlTag(students.get(i)) + "</td>");
                out.print("</tr>");
            }
        }
    %>
</table>

<a href="load_cookies.jsp">
    Load
</a>
<a href="save_cookies.jsp">
    Save
</a>
</body>
</html>
