<%@ page import="com.sda.university.Student" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 6/30/18
  Time: 3:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Zapisz</title>
</head>
<body>
<%
    String zmieniany_index = request.getParameter("zmieniany_index");

    String imie = request.getParameter("first_name");
    String nazwisko = request.getParameter("last_name");
    Integer wiek = Integer.parseInt(request.getParameter("age"));
    String index = request.getParameter("index");

    List<Student> students = (List<Student>) session.getAttribute("studenty");
    if (students == null) {
        students = new ArrayList<>();
    }

    Iterator<Student> studentIterator = students.iterator();
    while (studentIterator.hasNext()) {
        Student student = studentIterator.next();
        if (student.getIndex().equals(zmieniany_index)) {
            student.setFirstName(imie);
            student.setLastName(nazwisko);
            student.setAge(wiek);
            student.setIndex(index);
            break;
        }
    }
    session.setAttribute("studenty", students);

    response.sendRedirect("lista_studentow.jsp");

%>

</body>
</html>
