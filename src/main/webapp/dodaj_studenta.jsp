<%@ page import="java.util.List" %>
<%@ page import="com.sda.university.Student" %>
<%@ page import="java.util.ArrayList" %>
<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 6/30/18
  Time: 11:34 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Dodaj studenta</title>
</head>
<body>

<%
    // przypisanie wartości z formularza do zmiennej
    String imie = request.getParameter("first_name");
    String nazwisko = request.getParameter("last_name");
    Integer wiek = Integer.parseInt(request.getParameter("age"));
    String index = request.getParameter("index");

    // wypisanie wartości z formularza na stronę
//    out.print(request.getParameter("first_name"));

    // stworzenie obiektu studenta
    // 2. stworzenie obiektu Student z informacji z request'a
    Student student = new Student(imie, nazwisko, wiek, index);
    // 1. wyświetlamy dane studenta
    //    out.println(student);

    List<Student> students = (List<Student>) session.getAttribute("studenty");
    if (students == null) {
        students = new ArrayList<>();
    }

    students.add(student);

    // 3. dodanie studenta do sesji - całą listę
    session.setAttribute("studenty", students);

    // to nie ja ->

    // 4. przekierowanie na stronę 'lista_studentow.jsp' - strona lista studentów ma pobierać studentów z sesji (listę) i listować ją w tabeli
    response.sendRedirect("lista_studentow.jsp");

//    session.setAttribute("studenty", lista);
//    List lista = session.getAttribute("studenty");
    //
%>

<%--<jsp:forward page="lista_studentow.jsp" />--%>
</body>
</html>
