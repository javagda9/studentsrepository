<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 6/30/18
  Time: 10:35 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="com.sda.university.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Studenci</title>
</head>
<body>

<table>
    <%
        List<Student> students = new ArrayList<>();
        students.add(new Student("marian", "brzęczyszczykiewicz", 15, "1234"));
        students.add(new Student("olek", "wariat", 16, "1235"));
        students.add(new Student("gustaw", "marcinski", 17, "1236"));
        students.add(new Student("leon", "zawodowiec", 18, "1237"));

        for (Student s : students) {
            out.print("<tr>");
            out.print("<td>" + s.getFirstName() + "</td>");
            out.print("<td>" + s.getLastName() + "</td>");
            out.print("<td>" + s.getAge() + "</td>");
            out.print("<td>" + s.getIndex() + "</td>");
            out.print("</tr>");
        }
    %>
</table>

<%-- zadeklaruj listę, dodaj do niej kilku studentów i wypisz ich do html w tabelce --%>
</body>
</html>
