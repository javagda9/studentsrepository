<%@ page import="com.sda.university.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Base64" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="com.google.gson.internal.LinkedTreeMap" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 6/30/18
  Time: 1:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Load cookies</title>
</head>
<body>

<%!
    //    /**
//     * Metoda zamienia string (który stworzyliśmy w save_cookies.jsp na listę studentów
//     * @param cookieValue - string z cookie
//     * @return - lista studentów
//     */
//    public List<Student> convertBackFromCookie(String cookieValue) {
//        List<Student> studenty = new ArrayList<>();
//
//        // każdy student jest oddzielony od poprzedniego znakiem ':'
//        String[] students = cookieValue.split("/");
//        for (String student : students) {
//            // wszystkie wartości pól studentów oddzielone są znakiem ';'
//            String[] values = student.split(";");
//
//            // tworzymy instancję studenta
//            //                      FirstName   LastName   Age                          Index
//            studenty.add(new Student(values[0], values[1], Integer.parseInt(values[2]), values[3]));
//        }
//        return studenty;
//    }
    public List<Student> convertBackFromCookie(String cookieValue) {
        byte[] result = Base64.getDecoder().decode(cookieValue.getBytes());
        String jsonString = new String(result);

        Gson gson = new Gson();
        List<LinkedTreeMap> students = gson.fromJson(jsonString, List.class);

        List<Student> realStudents=  new ArrayList<>();
        for (LinkedTreeMap<String, Object> mapEntry: students) {
            realStudents.add(new Student(
                    String.valueOf(mapEntry.get("firstName")),
                    String.valueOf(mapEntry.get("lastName")),
                    (int)Double.parseDouble(String.valueOf(mapEntry.get("age"))),
                    String.valueOf(mapEntry.get("index"))));
        }
        return realStudents;
    }
%>
<%
    // pobieramy ciasteczka
    Cookie[] cookies = request.getCookies();

    // znaleźć ciasteczko ze studentami i załadować je do sesji
    for (int i = 0; i < cookies.length; i++) {
        if (cookies[i].getName().equals("saved_studenty")) {
            session.setAttribute("studenty", convertBackFromCookie(cookies[i].getValue()));
//            out.print(convertBackFromCookie(cookies[i].getValue()));
            break;
        }
    }
    response.sendRedirect("lista_studentow.jsp");
%>

</body>
</html>
