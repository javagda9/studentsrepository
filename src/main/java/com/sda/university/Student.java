package com.sda.university;

public class Student {
    private String firstName;
    private String lastName;
    private int age;
    private String index;

    public Student(String firstName, String lastName, int age, String index) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.index = index;
    }

    public Student() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }
}
