<%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 6/30/18
  Time: 10:19 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tabliczka mnozenia do 10</title>
</head>
<body>
<%! int entryCounter = 0; %>

<table>
    <%
        for (int i = 1; i < 11; i++) {
            // wiersz
            out.print("<tr>");
            for (int j = 1; j < 11; j++) {
                out.print("<td>");
                out.print(i * j);
                out.print("</td>");
            }
            out.print("</tr>");
        }
    %>
</table>

<hr>
Wejść na stronę: <%= entryCounter++ %>
<%--Wejść na stronę: <% out.print(entryCounter++); %>--%>
</body>
</html>
